+++
title = "example"
description = "Include collapsible examples of play/mechanics"
+++

You can include collapsible examples of play or to illustrate mechanics.

	{{%/* example "Francesca Duels Gregory" */%}}
	Body text of example
	+ Unordered List
	+ Another item

	Paragraph explaining things.

	Takeaways:
	1. This
	2. That
	3. The other
	{{%/* example */%}}

{{% example "Francesca Duels Gregory" %}}

**Body text of example**

+ Unordered List
+ Another item

_Paragraph explaining things._

Takeaways:

1. This
2. That
3. The other

{{% /example %}}