+++
title = "animal"
description = "Include stat blocks for animals but only write them once."
+++

You can define creatures that adventurers might encounter in a yaml file in the data directory.
For example, in `data/animals/bear.yaml` we have the following defined:

{{< highlight yaml >}}
Name: Bear
Characteristics:
- Body: 3D6 + 6 (17)
- Mind: 5
- Power: 3D6 (11)
Attributes:
- HP: 14
- PP: 11
Combat:
- Close:
  - Score: 60%
  - Bite: 1D8 + 3
  - Claw: 1D6 + 3
{{< /highlight >}}

Each animal needs to be defined only once in the data folder.
Once an animal is defined, it can be included in any page using the `animal` shortcode:

    {{%/* animal bear */%}}

Will render the defined animal to:

{{% animal bear %}}

{{% notice animal %}}
Text, awesome text.
{{% /notice %}}